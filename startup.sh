#!/bin/bash

chmod 777 /tmp

#fuse workaround --cap-add=sys_admin --device=/dev/fuse:/dev/fuse
chown root:fuse /dev/fuse
chmod 0660 /dev/fuse

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

: ${LDAP_AUTH:=0}
: ${LDAP_BASEDN:="dc=example,dc=com"}
: ${LDAP_SERVER_URL:="ldap://ldap.docker.local"}
: ${LDAP_ROOT_BINDDN="cn=admin,dc=example,dc=com"}
LDAP_GROUPS_MAPPING=( [0]="*;*;%admins;Al0000-2400;sudo,adm,lpadmin,fuse" [1]="*;*;%users;Al0000-2400;fuse,lpadmin" )

WINE_GID=401
WINEADMIN_GID=402

sed -i "s/^#UsePAM/UsePAM/g" /etc/ssh/sshd_config
sed -i "s/^UsePAM .*/UsePAM yes/g" /etc/ssh/sshd_config

groupadd -g ${WINE_GID} wine
groupadd -g ${WINEADMIN_GID} wineadmin
mkdir -p /var/lib/wine && chgrp ${WINEADMIN_GID} /var/lib/wine && chmod 2775 /var/lib/wine

if [ $LDAP_AUTH -eq 1 ];
then
	echo "ldap-auth-config	ldap-auth-config/bindpw	password" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/rootbindpw	password" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/move-to-debconf	boolean	true" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/binddn	string	cn=proxyuser,dc=example,dc=net" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/rootbinddn	string	${LDAP_ROOT_BINDDN}" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/dblogin	boolean	false" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/ldapns/ldap-server	string	${LDAP_SERVER_URL}" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/override	boolean	true" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/dbrootlogin	boolean	false" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/ldapns/ldap_version	select	3" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/pam_password	select	md5" | debconf-set-selections
	echo "ldap-auth-config	ldap-auth-config/ldapns/base-dn	string	${LDAP_BASEDN}" | debconf-set-selections

	printf "%s\n" "${LDAP_GROUPS_MAPPING[@]}" >> /etc/security/group.conf

	sed -i "s/^base .*/base ${LDAP_BASEDN}/g" /etc/ldap.conf
	sed -i "s#^uri .*#uri ${LDAP_SERVER_URL}#g" /etc/ldap.conf

	cat > /usr/share/pam-configs/ldap-mkhomedir <<EOF
Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
        required                        pam_mkhomedir.so umask=0022 skel=/etc/skel	
EOF

	cat > /usr/share/pam-configs/my_groups <<EOF
Name: activate /etc/security/group.conf
Default: yes
Priority: 900
Auth-Type: Primary
Auth:
	required                        pam_group.so use_first_pass
EOF

	auth-client-config -t nss -p lac_ldap
	pam-auth-update
	dpkg-reconfigure ldap-auth-config

	sudo killall -s KILL nscd
else
  [ -n "$TERM_GROUPS" ] && TERM_GROUPS="--groups ${TERM_GROUPS}"
  [ -n "$TERM_USER_ID" ] && TERM_USER_ID="--uid ${TERM_USER_ID}"

  useradd -m -U --shell /bin/bash ${TERM_USER_ID} ${TERM_GROUPS} "$TERM_USER"
  echo "$TERM_USER:$TERM_PASS" |chpasswd

  #echo "export LC_ALL=${D_LANG}.${D_CODEPAGE}" >> /home/${TERM_USER}/.bash_profile
  #echo "export LANG=${D_LANG}.${D_CODEPAGE}" >> /home/${TERM_USER}/.bash_profile

fi

update-locale LANG=${D_LANG}.${D_CODEPAGE} 

/etc/init.d/cups start

lpadmin -p Virtual_X2GO_Printer -v cups-x2go:/ -P /usr/share/ppd/cups-x2go/CUPS-X2GO.ppd -L location -E
lpadmin -d Virtual_X2GO_Printer

exec /usr/sbin/sshd -De
