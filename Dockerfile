FROM phusion/baseimage:latest

MAINTAINER Dmitry K "d.p.karpov@gmail.com"

ENV D_TIME_ZONE Europe/Moscow
ENV D_CODEPAGE UTF-8 
ENV D_LANG ru_RU

ENV ETCD_VER v2.0.8

# this forces dpkg not to call sync() after package extraction and speeds up install
RUN echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02apt-speedup
# we don't need and apt cache in a container
RUN echo "Acquire::http {No-Cache=True;};" > /etc/apt/apt.conf.d/no-cache

# Set the env variable DEBIAN_FRONTEND to noninteractive
ENV DEBIAN_FRONTEND noninteractive

RUN dpkg --add-architecture i386 && \ 
apt-get update && apt-get upgrade -y && \
apt-get install -y  wget python-software-properties software-properties-common && \
apt-add-repository multiverse && \
apt-add-repository universe && \
add-apt-repository ppa:ubuntu-wine/ppa && \
apt-add-repository ppa:ubuntu-mate-dev/ppa && \
apt-add-repository ppa:ubuntu-mate-dev/trusty-mate && \
add-apt-repository ppa:x2go/stable && \
add-apt-repository ppa:webupd8team/java && \
add-apt-repository ppa:libreoffice/ppa && \
sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5044912E && \
sudo add-apt-repository "deb http://linux.dropbox.com/ubuntu $(lsb_release -sc) main" && \
apt-get update && \
echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections; echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections && \
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections && \
apt-get install -y --no-install-recommends ubuntu-mate-core ubuntu-mate-desktop language-pack-gnome-ru language-selector-common hyphen-ru mythes-ru myspell-ru libreoffice-help-ru libreoffice-l10n-ru thunderbird-locale-ru cups cups-bsd cups-common \
sshfs x2goserver x2goserver-xsession x2goserver-printing cups-x2go x2gomatebindings \
cabextract p7zip unrar unzip wine1.7 zenity winbind \
ttf-mscorefonts-installer \
caja-dropbox dropbox \
mc \
oracle-java7-installer \
libpam-ldap nscd \
libfuse2 && \
cd /tmp ; apt-get download fuse && \
cd /tmp ; dpkg-deb -x fuse_* . && \
cd /tmp ; dpkg-deb -e fuse_* && \
cd /tmp ; rm fuse_*.deb && \
cd /tmp ; echo -en '#!/bin/bash\nexit 0\n' > DEBIAN/postinst && \
cd /tmp ; dpkg-deb -b . /fuse.deb && \
cd /tmp ; dpkg -i /fuse.deb && rm -rf /fuse.deb && \
cd /tmp ; wget https://launchpad.net/~gnome3-team/+archive/gnome3/+files/gnome-keyring_3.4.1-4ubuntu1~precise1_i386.deb && \
cd /tmp ; dpkg-deb -x gnome-keyring_* . && \
cd /tmp ; apt-get download p11-kit-modules:i386 && \
cd /tmp ; dpkg-deb -x p11-kit-modules_* . && \
mkdir -p /usr/lib/i386-linux-gnu/pkcs11 && \
cp /tmp/usr/lib/i386-linux-gnu/pkcs11/* /usr/lib/i386-linux-gnu/pkcs11 && \
apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/* && \
rm -rf /tmp/* \
/src \
/etc/xdg/autostart/update-notifier.desktop \
/etc/xdg/autostart/indicator-bluetooth.desktop \
/etc/xdg/autostart/blueman.desktop \
/etc/xdg/autostart/nm-applet.desktop \
/etc/xdg/autostart/nm-applet-mate.desktop \
/etc/cron.weekly/apt-xapian-index \
/etc/xdg/autostart/mate-screensaver.desktop \
/etc/xdg/autostart/mate-power-manager.desktop \
/etc/xdg/autostart/deja-dup-monitor.desktop \
/etc/xdg/autostart/at-spi-dbus-bus.desktop

RUN rm -rf /etc/service/*

# Upstart and DBus have issues inside docker. We work around in order to install firefox.
RUN dpkg-divert --local --rename --add /sbin/initctl && ln -sf /bin/true /sbin/initctl

# Set locale (fix the locale warnings)
RUN localedef -v -c -i ${D_LANG} -f ${D_CODEPAGE} ${D_LANG}.${D_CODEPAGE} || :
RUN update-locale LANG=${D_LANG}.${D_CODEPAGE}
RUN echo ${D_TIME_ZONE} > /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

ADD 10_gnome-desktop-lockdown.gschema.override /usr/share/glib-2.0/schemas/10_nxserver-gnome-desktop-lockdown.gschema.override
RUN glib-compile-schemas /usr/share/glib-2.0/schemas

RUN mkdir -p /var/run/sshd

RUN mkdir /src

ADD startup.sh /etc/service/x2go/run
RUN chmod +x /etc/service/x2go/run

ADD discovery.sh /etc/service/discovery/run
RUN chmod +x /etc/service/discovery/run

ADD nscd.sh /etc/service/nscd/run
RUN chmod +x /etc/service/nscd/run

ADD ./00_regen_ssh_host_keys.sh /etc/my_init.d/00_regen_ssh_host_keys.sh
RUN chmod +x /etc/my_init.d/00_regen_ssh_host_keys.sh

RUN curl -sSL https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks -o /usr/bin/winetricks && \
chmod 755 /usr/bin/winetricks

RUN curl -sSL https://github.com/coreos/etcd/releases/download/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /etcd-${ETCD_VER}-linux-amd64.tar.gz && \
cd /; tar -xf etcd-${ETCD_VER}-linux-amd64.tar.gz && \
cp /etcd-${ETCD_VER}-linux-amd64/etcdctl /usr/local/bin/ && \
rm -rf /etcd-${ETCD_VER}-linux-amd64*

#fixes for wine
RUN ln -fs /var/run/cups/printcap /etc/printcap
RUN chmod 777 /tmp

EXPOSE 22

CMD ["/sbin/my_init"]

