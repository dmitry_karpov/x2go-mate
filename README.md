Этот образ предназначен для запуска графического окружения Mate Desktop и доступа к нему по средствам терминального клиента x2go.
В базовый набор входят такие пакеты как:
 
* LibreOffice
* Firefox
* Thunderbird
* wine

Используя этот образ как базовый, можно создать свой, наполнив его необходимым ПО.

Существует несколько способов запустить Docker контейнер из этого образа:
* простой режим с указанием имени пользователя и паролем через переменные окружения;
* с использование LDAP-совместимой службы  каталогов, таких как: Active Directory, Open LDAP, Free IPA;

Пример запуска контейнера:
`docker run -d -m="1024m" --name x2go_test -p 2022:22 \
--cap-add=sys_admin --device=/dev/fuse:/dev/fuse \
-e TERM_USER=username -e TERM_PASS=password -e TERM_GROUPS=sudo,adm,lpadmin,fuse \
intersoftlab/x2go-mate`

Список переменных окружения, передаваемых при запуске контейнера:

- `TERM_USER`
- `TERM_PASS`
- `TERM_GROUPS`

- `LDAP_AUTH`
- `LDAP_BASEDN`
- `LDAP_SERVER_URL`
- `LDAP_ROOT_BINDDN`
- `LDAP_GROUPS_MAPPING`

- `D_TIME_ZONE`
- `D_CODEPAGE` 
- `D_LANG`


