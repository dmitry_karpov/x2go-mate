#!/bin/bash

DAEMON="/usr/sbin/nscd"

[ -d /var/run/nscd ] || mkdir -p /var/run/nscd

$DAEMON -F