#!/bin/bash


while [ ! -f /.public_ip ]
do
  sleep 1
done

PUBLIC_HOST=$(cat /.public_ip)
#PUBLIC_PORT
: ${SET_TTL:=30}
: ${ETCDCTL_CMD:="etcdctl -C http://172.17.42.1:4001"}

#function get_port {
#	GO_TPL='{{ range $port, $val := .HostConfig.PortBindings }}{{ if eq $port "'$1'"}}{{ (index $val 0).HostPort }}{{ end }} {{ end }}'
#	echo $(docker inspect --format "${GO_TPL}" $2)
#}

function get_cid {
	echo $(docker ps | grep $1 | awk 'BEGIN { FS = " " } ; { print $1 }')
}

function set_kv_ttl {
	$ETCDCTL_CMD set --ttl $3 $1 $2
}

function etcd_set_or_update_dir {
	$ETCDCTL_CMD setdir --ttl $2  $1 >/dev/null 2>&1 || etcdctl updatedir --ttl $2  $1 >/dev/null 2>&1
}

$ETCDCTL_CMD rm --recursive /x2go/nodes/${PUBLIC_HOST}

while [[ true ]]; do

	etcd_set_or_update_dir /x2go/nodes/${PUBLIC_HOST} $SET_TTL

	etcd_set_or_update_dir /x2go/nodes/${PUBLIC_HOST}/sessions $SET_TTL
	set_kv_ttl /x2go/nodes/${PUBLIC_HOST}/commands/MATE MATE 0 
			
	for SESSION in $(x2golistsessions_root); do
		set_kv_ttl "/x2go/nodes/${PUBLIC_HOST}/sessions/$(echo ${SESSION} |awk 'BEGIN { FS = "|" } ; { print $2 }')" $(echo $SESSION |  sed "s/[^|]*/${PUBLIC_HOST}/"4) $SET_TTL
	done
			
	sleep 2
done
